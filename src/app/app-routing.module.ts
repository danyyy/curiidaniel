import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DialogoComponent } from './components/dialogo/dialogo.component';
import { AgendaComponent } from './components/agenda/agenda.component';





const routes: Routes = [ 

  { path: 'agenda', component: AgendaComponent },
  { path: 'dialogo', component: DialogoComponent },  
  { path: '**', pathMatch: 'full', redirectTo: 'agenda' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
